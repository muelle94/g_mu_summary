# a<sub>&mu;</sub> status plot

## `g_mu_summary2021.C` (April 2021 version)

This macro gives the status of a<sub>&mu;</sub> just after the [FERMILAB seminar on
April 7, 2021](https://theory.fnal.gov/events/event/first-results-from-the-muon-g-2-experiment-at-fermilab/). 

The theoretical values use the different
contributions as given in the
[White Paper](https://arxiv.org/pdf/2006.04822.pdf) of the
[Theory Initiative](https://muon-gm2-theory.illinois.edu/). Since the leading
order hadronic contribution is dominating the uncertainty of the
theoretical values, several values for a$_\mu$ are plotted which use the
different evaluations for the leading order hadronic contribution given in
Table 4 of the White Paper as well as the White Paper average.

To run the macro in ROOT:

> `.L g_mu_summary2021.C`  
> `g_mu_summary()`

You can then save the plot in different formats by selecting `Save as...`
from the `File` menu.

<img src="g_mu_summary.png" alt="g_mu_summary.png" width="200"/>

## `g_mu_summary2023.C` (August 2023 version)

This macro gives the status of a<sub>&mu;</sub> after the [FERMILAB Wine and Cheese seminar on August 10, 2023](https://indico.fnal.gov/event/60738/).

For the theory, only the results from the White Paper and the [BMW20 result](http://de.arxiv.org/pdf/2002.12347v3) from lattice QCD are presented.

Update in March 2024: I added Fred Jegerlehner's evaluation as found in his
alphaQED23-code package at [https://people.physik.hu-berlin.de/~fjeger/alphaQEDc23.tar.gz](https://people.physik.hu-berlin.de/~fjeger/alphaQEDc23.tar.gz).

To run the macro in ROOT:

> `.L g_mu_summary2023.C+`  
> `g_mu_summary()`

<img src="g_mu_summary2023.png" alt="g_mu_summary2023.png" width="200"/>

docs:

BNL_muplus_0208001.pdf (BNL 2002 result for mu+)

BNL_muminus_0401008.pdf (BNL 2002 result for mu-)

Roberts_PHIPSI09.pdf (BNL update of average with newer lambda)

1_BLRoberts-g-2-FNAL-v2.ppt (Lee Roberts PHIPSI09 presentation)

