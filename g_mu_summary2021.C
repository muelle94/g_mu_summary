#include <stdio.h>
#include <iostream>
#include <fstream>

#include <TStyle.h>
#include <TCanvas.h>
#include <TGraphErrors.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TGaxis.h>
#include <TH2.h>
#include <TBox.h>
#include <TColor.h>
#include <TLatex.h>

#include <string.h>

void g_mu_summary() {

  gStyle->SetEndErrorSize(8);
  
  //axis values (in units of 10^{-10}!)
  Double_t Offset = 11659000.;
  Double_t xmin = 135.;
  Double_t xmax = 230.;
  
  constexpr Int_t nPoints = 11.;
  Double_t ymin = 0.;
  Double_t ymax = nPoints+1.;

  Double_t zero[1] = {0.};

  // Canvas definition:
   Double_t w = 700;
   Double_t h = 700;
  TCanvas *c1 = new TCanvas("c1","(g-2) summary",700,700);
  //c1->SetWindowSize(w + (w - c1->GetWw()), h + (h - c1->GetWh()));
  c1->Range(130,-1.5,235,nPoints+1.5);
  
  //Color definitions:
  Int_t ci_lG = TColor::GetFreeColorIndex();
  TColor *lightGrey = new TColor(ci_lG, 0.93, 0.93, 0.93);

  Int_t ci_lGreen = TColor::GetFreeColorIndex();
  TColor *lightGreen = new TColor(ci_lGreen, 0.81, 1., 0.71);
  
  Int_t ci_mG = TColor::GetFreeColorIndex();
  TColor *mediumGreen = new TColor(ci_mG, 0.3, 0.7, 0.);    
  
  Int_t ci_dG = TColor::GetFreeColorIndex();
  TColor *darkGreen = new TColor(ci_dG, 0., 0.3, 0.);  

  Int_t ci_Tq = TColor::GetFreeColorIndex();
  TColor *Turqoise = new TColor(ci_Tq, 0.61, 1., 0.81); 
  


  //======================= Values are given here: =================
  //common QED, weak + hlbl values (in units of 10^{-10}):
  Double_t QED = 11658471.8931; //using alpha(Cs)
  Double_t dQED = 0.0104;
  
  Double_t EW = 15.36;
  Double_t dEW = 0.1;

  Double_t HadNLO = -9.83;
  Double_t dHadNLO=  0.07;

  Double_t HadNNLO = 1.24;
  Double_t dHadNNLO = 0.01;

  Double_t HadLBL = 9.2;
  Double_t dHadLBL = 1.8;

  Double_t sum = QED + EW + HadNLO + HadNNLO + HadLBL;
  //Uncorrelated uncertainties added in quadrature:
  Double_t dsum2 = dQED*dQED + dEW*dEW + dHadLBL*dHadLBL;
  //Fully (anti)correlated had. higher orders added linearly: 
  Double_t dsum = -dHadNLO +dHadNNLO;

  
  //FJ17 value of HadLO (in units of 10^{-10}):
  Double_t FJ17[1] = {688.1 + sum - Offset};
  Double_t dFJ17[1] = {sqrt((4.1+dsum)*(4.1+dsum) + dsum2)};
  Double_t yFJ17[1] = {nPoints};

  //BDJ19 value of HadLO (in units of 10^{-10}):
  Double_t BDJ19[1] = {687.1 + sum - Offset};
  Double_t dBDJ19[1] = {sqrt((3.0+dsum)*(3.0+dsum)  + dsum2)};
  Double_t yBDJ19[1] = {nPoints-1.};

  //DHMZ19 value of HadLO (in units of 10^{-10}):
  Double_t DHMZ19[1] = {694.0 + sum - Offset};
  Double_t dDHMZ19[1]= {sqrt((4.0+dsum)*(4.0+dsum) + dsum2)};
  Double_t yDHMZ19[1] = {nPoints-2};

  //KNT19 value of HadLO (in units of 10^{-10}):
  Double_t KNT19[1] = {692.8 + sum - Offset};
  Double_t dKNT19[1]= {sqrt((2.4+dsum)*(2.4+dsum) +dsum2)};
  Double_t yKNT19[1] = {nPoints-3};
  
  //White Paper consensus value of HadLO (in units of 10^{-10}):
  Double_t WP20[1] = {693.1 + sum - Offset};
  Double_t dWP20[1]= {sqrt((4.0+dsum)*(4.0+dsum) + dsum2)};
  Double_t yWP20[1] = {nPoints-4.};


  cout << "WP consensus (in units of 10^{-10}): " << WP20[0] << " pm " << dWP20[0] << endl;;

  //BNL-E821 2002 mu+ result (in units of 10^{-10}):
  Double_t BNL2002[1] = {11659203.0 - Offset};
  Double_t dBNL2002[1] = {8.};
  Double_t yBNL2002[1] = {5.};

  //BNL-E821 2004 mu- result (in units of 10^{-10}):
  Double_t BNL2004[1] = {11659214.0 - Offset};
  Double_t dBNL2004[1] = {8.5};
  Double_t yBNL2004[1] = {4.};  
  
  //BNL-E821 average (in units of 10^{-10}):
  Double_t BNLave[1] = {11659208.9 - Offset};
  Double_t dBNLave[1] = {6.3};
  Double_t yBNLave[1] = {3.};

   //FNAL-E821 run 1 (in units of 10^{-10}):
  Double_t FNAL2021[1] = {11659204.0 - Offset};
  Double_t dFNAL2021[1] = {5.4};
  Double_t yFNAL2021[1] = {2.}; 

   //FNAL-E821 run 1 + BNL combined(in units of 10^{-10}):
  Double_t FNAL_BNL[1] = {11659206.1 - Offset};
  Double_t dFNAL_BNL[1] = {4.1};
  Double_t yFNAL_BNL[1] = {1.};   
  
  //Boxes
  //Light grey box under experimental value(s):
  TBox *boxExp = new TBox(xmin, 0., xmax, 6.);
  boxExp->SetFillColor(ci_lG);

  //Light green box depicting uncertainty of BNL result:
  TBox *boxBNL = new TBox(BNLave[0]-dBNLave[0], 0., BNLave[0]+dBNLave[0], nPoints+1.);
  boxBNL->SetFillColor(ci_lGreen);
  
  //Light green box depicting uncertainty of FNAL run1 + BNL result:
  TBox *boxFNAL_BNL = new TBox(FNAL_BNL[0]-dFNAL_BNL[0], 0., FNAL_BNL[0]+dFNAL_BNL[0], nPoints+1.);
  boxFNAL_BNL->SetFillColor(ci_lGreen);
  
  // Axes on all 4 sides, only bottom one has labels:
  
  TGaxis *axisXd = new TGaxis(xmin,0.,xmax,0.,xmin,xmax,510,"+");
  axisXd->SetName("axisXd");
  axisXd->SetLineWidth(2.);
  axisXd->SetLabelSize(0.03);
  axisXd->SetTitleSize(0.04);
  axisXd->SetTitle("a_{#mu} - 11 659 000   [10^{-10}]      ");
  

  TGaxis *axisXu = new TGaxis(xmin,nPoints+1,xmax,nPoints+1,xmin,xmax,510,"U-");
  axisXu->SetName("axisXu");
  axisXu->SetLineWidth(2.);
  
  
  TGaxis *axisYl = new TGaxis(xmin,0.,xmin,nPoints+1.,0.,nPoints+1.,0,"");
  axisYl->SetName("axisYl");
  axisYl->SetLineWidth(2.);
  

   TGaxis *axisYr = new TGaxis(xmax,0.,xmax,nPoints+1.,0.,nPoints+1,0,"");
  axisYr->SetName("axisYr");
  axisYr->SetLineWidth(2.);

  //Plotting FJ17 result:
  TGraphErrors *grFJ17 = new TGraphErrors(1,FJ17,yFJ17,dFJ17,zero);
  grFJ17->SetLineWidth(2.);
  grFJ17->SetMarkerSize(1.8);
  grFJ17->SetMarkerStyle(22);
  grFJ17->SetLineColor(kBlue);
  grFJ17->SetMarkerColor(kBlue);

  TText *tFJ17_label = new TText(138.,yFJ17[0]+0.15, "FJ17");
  tFJ17_label->SetTextColor(kBlue);
  tFJ17_label->SetTextSize(22);
  tFJ17_label->SetTextFont(63);
  tFJ17_label->SetTextAlign(kHAlignLeft+kVAlignBottom);

  string sFJ17_value = Form("%4.1f %s %4.1f",FJ17[0],"#pm",dFJ17[0]);
  
  TLatex *tFJ17_value = new TLatex(138.,yFJ17[0]-0.25,sFJ17_value.c_str() );
  tFJ17_value->SetTextColor(kBlue);
  tFJ17_value->SetTextSize(18);
  tFJ17_value->SetTextFont(63);
  tFJ17_value->SetTextAlign(kHAlignLeft+kVAlignBottom);

  //Plotting BDJ19 result:
  TGraphErrors *grBDJ19 = new TGraphErrors(1,BDJ19,yBDJ19,dBDJ19,zero);
  grBDJ19->SetLineWidth(2.);
  grBDJ19->SetMarkerSize(1.8);
  grBDJ19->SetMarkerStyle(22);
  grBDJ19->SetLineColor(kBlue);
  grBDJ19->SetMarkerColor(kBlue);

  TText *tBDJ19_label = new TText(138.,yBDJ19[0]+0.15, "BDJ19");
  tBDJ19_label->SetTextColor(kBlue);
  tBDJ19_label->SetTextSize(22);
  tBDJ19_label->SetTextFont(63);

  string sBDJ19_value = Form("%4.1f %s %4.1f",BDJ19[0],"#pm",dBDJ19[0]);
  
  TLatex *tBDJ19_value = new TLatex(138.,yBDJ19[0]-0.25,sBDJ19_value.c_str() );
  tBDJ19_value->SetTextColor(kBlue);
  tBDJ19_value->SetTextSize(18);
  tBDJ19_value->SetTextFont(63);

  //Plotting DHMZ19 result:
  TGraphErrors *grDHMZ19 = new TGraphErrors(1,DHMZ19,yDHMZ19,dDHMZ19,zero);
  grDHMZ19->SetLineWidth(2.);
  grDHMZ19->SetMarkerSize(1.8);
  grDHMZ19->SetMarkerStyle(22);
  grDHMZ19->SetLineColor(kBlue);
  grDHMZ19->SetMarkerColor(kBlue);

  TText *tDHMZ19_label = new TText(138.,yDHMZ19[0]+0.15, "DHMZ19");
  tDHMZ19_label->SetTextColor(kBlue);
  tDHMZ19_label->SetTextSize(22);
  tDHMZ19_label->SetTextFont(63);

  string sDHMZ19_value = Form("%4.1f %s %4.1f",DHMZ19[0],"#pm",dDHMZ19[0]);
  
  TLatex *tDHMZ19_value = new TLatex(138.,yDHMZ19[0]-0.25,sDHMZ19_value.c_str() );
  tDHMZ19_value->SetTextColor(kBlue);
  tDHMZ19_value->SetTextSize(18);
  tDHMZ19_value->SetTextFont(63);

  //Plotting KNT19 result:
  TGraphErrors *grKNT19 = new TGraphErrors(1,KNT19,yKNT19,dKNT19,zero);
  grKNT19->SetLineWidth(2.);
  grKNT19->SetMarkerSize(1.8);
  grKNT19->SetMarkerStyle(22);
  grKNT19->SetLineColor(kBlue);
  grKNT19->SetMarkerColor(kBlue);

  TText *tKNT19_label = new TText(138.,yKNT19[0]+0.15, "KNT19");
  tKNT19_label->SetTextColor(kBlue);
  tKNT19_label->SetTextSize(22);
  tKNT19_label->SetTextFont(63);

  string sKNT19_value = Form("%4.1f %s %4.1f",KNT19[0],"#pm",dKNT19[0]);
  
  TLatex *tKNT19_value = new TLatex(138.,yKNT19[0]-0.25,sKNT19_value.c_str() );
  tKNT19_value->SetTextColor(kBlue);
  tKNT19_value->SetTextSize(18);
  tKNT19_value->SetTextFont(63);

  //Plotting WP20 result:
  TGraphErrors *grWP20 = new TGraphErrors(1,WP20,yWP20,dWP20,zero);
  grWP20->SetLineWidth(2.);
  grWP20->SetMarkerSize(1.5);
  grWP20->SetMarkerStyle(20);
  grWP20->SetLineColor(kBlack);
  grWP20->SetMarkerColor(kBlack);

  TText *tWP20_label = new TText(138.,yWP20[0]+0.15, "Muon g-2 TI 2020");
  tWP20_label->SetTextColor(kBlack);
  tWP20_label->SetTextSize(22);
  tWP20_label->SetTextFont(63);

  string sWP20_value = Form("%4.1f %s %4.1f",WP20[0],"#pm",dWP20[0]);
  
  TLatex *tWP20_value = new TLatex(138.,yWP20[0]-0.25,sWP20_value.c_str() );
  tWP20_value->SetTextColor(kBlack);
  tWP20_value->SetTextSize(18);
  tWP20_value->SetTextFont(63);

  //Plotting BNL 2002 result:
  TGraphErrors *grBNL2002 = new TGraphErrors(1,BNL2002,yBNL2002,dBNL2002,zero);
  grBNL2002->SetLineWidth(2.);
  grBNL2002->SetMarkerSize(1.5);
  grBNL2002->SetMarkerStyle(21);
  grBNL2002->SetLineColor(ci_dG);
  grBNL2002->SetMarkerColor(ci_dG);
   
  TLatex *tBNL2002_label = new TLatex(138.,yBNL2002[0]+0.15, "BNL-E821 2002 (#mu^{+})");
  tBNL2002_label->SetTextColor(ci_dG);
  tBNL2002_label->SetTextSize(22);
  tBNL2002_label->SetTextFont(63);

  string sBNL2002_value = Form("%4.1f %s %4.1f",BNL2002[0],"#pm",dBNL2002[0]);
  
  TLatex *tBNL2002_value = new TLatex(138.,yBNL2002[0]-0.25,sBNL2002_value.c_str() );
  tBNL2002_value->SetTextColor(ci_dG);
  tBNL2002_value->SetTextSize(18);
  tBNL2002_value->SetTextFont(63);

  //Plotting BNL 2004 result:
  TGraphErrors *grBNL2004 = new TGraphErrors(1,BNL2004,yBNL2004,dBNL2004,zero);
  grBNL2004->SetLineWidth(2.);
  grBNL2004->SetMarkerSize(1.5);
  grBNL2004->SetMarkerStyle(21);
  grBNL2004->SetLineColor(ci_dG);
  grBNL2004->SetMarkerColor(ci_dG);
   
  TLatex *tBNL2004_label = new TLatex(138.,yBNL2004[0]+0.15, "BNL-E821 2004 (#mu^{-})");
  tBNL2004_label->SetTextColor(ci_dG);
  tBNL2004_label->SetTextSize(22);
  tBNL2004_label->SetTextFont(63);

  string sBNL2004_value = Form("%4.1f %s %4.1f",BNL2004[0],"#pm",dBNL2004[0]);
  
  TLatex *tBNL2004_value = new TLatex(138.,yBNL2004[0]-0.25,sBNL2004_value.c_str() );
  tBNL2004_value->SetTextColor(ci_dG);
  tBNL2004_value->SetTextSize(18);
  tBNL2004_value->SetTextFont(63);
  
  //Plotting BNL result:
  TGraphErrors *grBNL = new TGraphErrors(1,BNLave,yBNLave,dBNLave,zero);
  grBNL->SetLineWidth(2.);
  grBNL->SetMarkerSize(1.5);
  grBNL->SetMarkerStyle(21);
  grBNL->SetLineColor(ci_dG);
  grBNL->SetMarkerColor(ci_dG);
   
  TLatex *tBNL_label = new TLatex(138.,yBNLave[0]+0.15, "BNL-E821 ave. (#mu^{+} + #mu^{-})");
  tBNL_label->SetTextColor(ci_dG);
  tBNL_label->SetTextSize(22);
  tBNL_label->SetTextFont(63);

  string sBNL_value = Form("%4.1f %s %4.1f",BNLave[0],"#pm",dBNLave[0]);
  
  TLatex *tBNL_value = new TLatex(138.,yBNLave[0]-0.25,sBNL_value.c_str() );
  tBNL_value->SetTextColor(ci_dG);
  tBNL_value->SetTextSize(18);
  tBNL_value->SetTextFont(63);

//Plotting FNAL 2021 (run1) result:
  TGraphErrors *grFNAL2021 = new TGraphErrors(1,FNAL2021,yFNAL2021,dFNAL2021,zero);
  grFNAL2021->SetLineWidth(2.);
  grFNAL2021->SetMarkerSize(1.5);
  grFNAL2021->SetMarkerStyle(21);
  grFNAL2021->SetLineColor(kRed);
  grFNAL2021->SetMarkerColor(kRed);
   
  TLatex *tFNAL2021_label = new TLatex(138.,yFNAL2021[0]+0.15, "FNAL-E989 2021 (#mu^{+})");
  tFNAL2021_label->SetTextColor(kRed);
  tFNAL2021_label->SetTextSize(22);
  tFNAL2021_label->SetTextFont(63);

  string sFNAL2021_value = Form("%4.1f %s %4.1f",FNAL2021[0],"#pm",dFNAL2021[0]);
  
  TLatex *tFNAL2021_value = new TLatex(138.,yFNAL2021[0]-0.25,sFNAL2021_value.c_str() );
  tFNAL2021_value->SetTextColor(kRed);
  tFNAL2021_value->SetTextSize(18);
  tFNAL2021_value->SetTextFont(63);

//Plotting FNAL run1 + BNL combined result:
  TGraphErrors *grFNAL_BNL = new TGraphErrors(1,FNAL_BNL,yFNAL_BNL,dFNAL_BNL,zero);
  grFNAL_BNL->SetLineWidth(2.);
  grFNAL_BNL->SetMarkerSize(1.5);
  grFNAL_BNL->SetMarkerStyle(21);
  grFNAL_BNL->SetLineColor(kRed);
  grFNAL_BNL->SetMarkerColor(kRed);
   
  TText *tFNAL_BNL_label = new TText(138.,yFNAL_BNL[0]+0.15, "FNAL+BNL comb.");
  tFNAL_BNL_label->SetTextColor(kRed);
  tFNAL_BNL_label->SetTextSize(22);
  tFNAL_BNL_label->SetTextFont(63);

  string sFNAL_BNL_value = Form("%4.1f %s %4.1f",FNAL_BNL[0],"#pm",dFNAL_BNL[0]);
  
  TLatex *tFNAL_BNL_value = new TLatex(138.,yFNAL_BNL[0]-0.25,sFNAL_BNL_value.c_str() );
  tFNAL_BNL_value->SetTextColor(kRed);
  tFNAL_BNL_value->SetTextSize(18);
  tFNAL_BNL_value->SetTextFont(63);  
  
  //Central line(s):
  TLine *lineBNL_value = new TLine(BNLave[0], 0., BNLave[0], nPoints+1.);
  lineBNL_value->SetLineColor(ci_mG);
  lineBNL_value->SetLineStyle(7);

  TLine *lineFNAL_BNL_value = new TLine(FNAL_BNL[0], 0., FNAL_BNL[0], nPoints+1.);
  lineFNAL_BNL_value->SetLineColor(ci_mG);
  lineFNAL_BNL_value->SetLineStyle(7);
  
  //Uncertainty lines
  TLine *lineBNL_high = new TLine(BNLave[0]+dBNLave[0], 0., BNLave[0]+dBNLave[0], nPoints+1.);
  lineBNL_high->SetLineColor(ci_mG);
  TLine *lineBNL_low = new TLine(BNLave[0]-dBNLave[0], 0., BNLave[0]-dBNLave[0], nPoints+1.);
  lineBNL_low->SetLineColor(ci_mG);

  TLine *lineFNAL_BNL_high = new TLine(FNAL_BNL[0]+dFNAL_BNL[0], 0., FNAL_BNL[0]+dFNAL_BNL[0], nPoints+1.);
  lineFNAL_BNL_high->SetLineColor(ci_mG);
  TLine *lineFNAL_BNL_low = new TLine(FNAL_BNL[0]-dFNAL_BNL[0], 0., FNAL_BNL[0]-dFNAL_BNL[0], nPoints+1.);
  lineFNAL_BNL_low->SetLineColor(ci_mG);
  
  //Box text:
  TText *tBNL_box = new TText(209.6,nPoints-1., "BNL-E821");
  tBNL_box->SetTextColor(ci_Tq);
  tBNL_box->SetTextSize(47);
  tBNL_box->SetTextFont(63);
  tBNL_box->SetTextAngle(270);

  TText *tFNAL_BNL_box = new TText(206.6,nPoints-1., "FNAL + BNL");
  tFNAL_BNL_box->SetTextColor(ci_Tq);
  tFNAL_BNL_box->SetTextSize(34);
  tFNAL_BNL_box->SetTextFont(63);
  tFNAL_BNL_box->SetTextAngle(270);
  
  // Here finally draw things:
  boxExp->Draw("same");
  //boxBNL->Draw("same");
  boxFNAL_BNL->Draw("same");
  //tBNL_box->Draw("same");
  tFNAL_BNL_box->Draw("same");
  
  axisXd->Draw("");
  axisXu->Draw("B");
  axisYl->Draw();
  axisYr->Draw();

  /*
  lineBNL_value->Draw("same");
  lineBNL_high->Draw("same");
  lineBNL_low->Draw("same");  
  */
  lineFNAL_BNL_value->Draw("same");
  lineFNAL_BNL_high->Draw("same");
  lineFNAL_BNL_low->Draw("same");
  

  
  grFJ17->Draw("P,same");
  tFJ17_label->Draw("same");
  tFJ17_value->Draw("same");  

  grBDJ19->Draw("P,same");
  tBDJ19_label->Draw("same");
  tBDJ19_value->Draw("same");  

  grDHMZ19->Draw("P,same");
  tDHMZ19_label->Draw("same");
  tDHMZ19_value->Draw("same");  

  grKNT19->Draw("P,same");
  tKNT19_label->Draw("same");
  tKNT19_value->Draw("same");  

  grWP20->Draw("P,same");
  tWP20_label->Draw("same");
  tWP20_value->Draw("same");  

  grBNL2002->Draw("PE1,same");
  tBNL2002_label->Draw("same");
  tBNL2002_value->Draw("same");

  grBNL2004->Draw("PE1,same");
  tBNL2004_label->Draw("same");
  tBNL2004_value->Draw("same");
  
  grBNL->Draw("PE1,same");
  tBNL_label->Draw("same");
  tBNL_value->Draw("same");
  
  grFNAL2021->Draw("PE1,same");
  tFNAL2021_label->Draw("same");
  tFNAL2021_value->Draw("same");

  grFNAL_BNL->Draw("PE1,same");
  tFNAL_BNL_label->Draw("same");
  tFNAL_BNL_value->Draw("same");
  
}
